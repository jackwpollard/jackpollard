<?php

$league_ids = [
    2021, // Premier League
    2016, // Championship
    2015, // League 1
    2002, // Bundesliga
    2019, // Serie A
    2003, // Eredivisie
    2017, // Premeira Liga
    2014, // La Liga
];

if (!in_array($_GET['id'], $league_ids)) {
	return;
}

print file_get_contents("{$_GET['id']}.json");