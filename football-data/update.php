<?php

if (php_sapi_name() !== "cli") {
    exit;
}

$league_ids = [
    2021, // Premier League
    2016, // Championship
    2015, // League 1
    2002, // Bundesliga
    2019, // Serie A
    2003, // Eredivisie
    2017, // Premeira Liga
    2014, // La Liga
];

$reqConfig = [];
$reqConfig['http']['method'] = 'GET';
$reqConfig['http']['header'] = 'X-Auth-Token: ' . '8b5d7afb0bac47e4986d111df36aada5';

foreach ($league_ids as $league_id) {
    $response = file_get_contents(
        "https://api.football-data.org/v2/competitions/{$league_id}/standings",
        false, 
        stream_context_create($reqConfig));

    file_put_contents(dirname(__FILE__) . "/{$league_id}.json", $response);

    print "Updated {$league_id}.json";

    sleep(10);
}