var gulp = require('gulp');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
// var livereload = require('gulp-livereload');
var concat = require('gulp-concat');

var sassPaths = [
  'node_modules/foundation-sites/scss'
];

gulp.task('sass', function() {
  return gulp.src(['app.scss'
                   // 'assets/plugins/responsive-nav/responsive-nav.css',
                   // 'assets/plugins/owl-carousel-2/owl.carousel.min.css',
                   // 'assets/plugins/owl-carousel-2/owl.theme.default.min.css'
                  ])
    .pipe(sass({
      includePaths: sassPaths
    })
    .on('error', sass.logError))
    // .pipe($.autoprefixer({
    //   browsers: ['last 2 versions', 'ie >= 9']
    // }))
    // .pipe(minifycss())
    .pipe(concat('style.css'))
    .pipe(gulp.dest(''))
    // .pipe(livereload());
});


gulp.task('watch', function(){
  // livereload.listen();

  // gulp.watch('**/*.php').on('change', function(file) {
  //   gulp.src(file.path).pipe(livereload());
  // });

  gulp.watch("*.scss", ['sass']);
});

gulp.task('default', ['sass', 'watch']);